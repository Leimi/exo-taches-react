import TaskItem from "./TaskItem";

export default function TaskList({ items, onItemDelete }) {
	return (
		<ul className="TaskList">
			{items.map((name, index) => (
				<li key={index}>
					<TaskItem
						id={`taskItem-${index}`}
						onDelete={() => {
							onItemDelete(index)
						}}
					>
						{name}
					</TaskItem>
				</li>
			))}
		</ul>
	)
}