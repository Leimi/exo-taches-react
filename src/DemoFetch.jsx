import { useState, useEffect } from "react";
// exo : créer un composant qui affiche les images des personnages principaux
// de Rick & Morty en s'aidant de cette url d'API :
// https://rickandmortyapi.com/api/character/1,2,3,4,5

export default function Demo() {
	const [people, setPeople] = useState([])

	useEffect(() => {
		fetch('https://rickandmortyapi.com/api/character/1,2,3,4,5').then((response) => {
			return response.json()
		}).then((json) => {
			setPeople(json)
		})
	}, [])
	
	return (
		<div>
			{ people.length
				? people.map(({name, image}) => (
					<div>
						<img key={name} src={image} alt={name} />
						<p>{name}</p>
					</div>
				))
				: "Chargement"
			}
		</div>
	)
}
