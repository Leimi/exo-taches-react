import Title from "./Title";
import Header from "./Header";
import NewTaskForm from "./NewTaskForm";
import TaskList from "./TaskList";
import Footer from "./Footer";
import { useState } from "react";

export default function App() {
	const [items, setItems] = useState([
		"Découvrir React",
		"Découvrir les props",
		"Découvrir la prop children"
	])
	const addItem = (item) => {
		setItems([
			...items,
			item
		])
	}
	const removeItem = (indexToRemove) => {
		items.splice(indexToRemove, 1)
		setItems([...items])
	}

	return (
		<div className="App">
			<div className="App-header">
				<Header />
			</div>

			<main role="main" className="App-content Gutter">
				<Title>Ajouter une tâche</Title>
				<NewTaskForm onAddTask={addItem} />

				<Title>Liste des tâches</Title>
				<TaskList items={items} onItemDelete={removeItem} />
			</main>

			<div className="App-footer">
				<Footer />
			</div>
		</div>
	)
}
