import { useEffect, useState } from "react";

export default function FetchRandMImage() {
	const characters = ['rick', 'morty', 'jerry', 'beth']
	const [inputValue, setInputValue] = useState("")
	const [image, setImage] = useState(null)

	const onInputChange = (event) => {
		setInputValue(event.target.value)
	}

	useEffect(() => {
		setImage(null)
		if (characters.includes(inputValue)) {
			fetch(`https://rickandmortyapi.com/api/character/?name=${inputValue}&status=alive`).then(response => {
				return response.json()
			}).then(json => {
				setImage(json.results[0].image);
			})
		}
	}, [inputValue])

	return (
		<div>
			<input
				type="text"
				onChange={onInputChange}
				value={inputValue}
			/>
			<div>
				{ image
					? <img src={image} alt="" />
					: <p>Renseignez un nom de personnage pour voir son image</p>
				}
			</div>
		</div>	
	)
}