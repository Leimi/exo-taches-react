import { useState } from "react";

export default function TaskItem({ id, onDelete, children }) {
	const onClickDelete = () => {
		onDelete()
	}
	const [checked, setChecked] = useState(false)
	return (
		<div className={checked ? "TaskItem TaskItem--done" : "TaskItem"}>
			<input
				type="checkbox"
				id={id}
				className="TaskItem-checkbox"
				checked={checked}
				onChange={(event) => {
					setChecked(event.target.checked)
				}}
			/>
			<label htmlFor={id} className="TaskItem-title">
				{ children }
			</label>
			<button
				type="button"
				className="TaskItem-deleteButton"
				onClick={onClickDelete}
			>
				Supprimer
			</button>
		</div>
	)
}