import { useState } from "react"

export default function NewTaskForm({ onAddTask }) {
	const [inputValue, setInputValue] = useState('')
	const [showSubmitErrors, setShowSubmitErrors] = useState(false)

	const onFormSubmit = (event) => {
		event.preventDefault()
		setShowSubmitErrors(true)
		if (inputValue) {
			onAddTask(inputValue)
			setShowSubmitErrors(false)
		}
		setInputValue("")
	}

	return (
		<form action="#" className="NewTaskForm" onSubmit={onFormSubmit}>
			<label htmlFor="new-task-name">Tâche</label>
			<input
				type="text"
				name="new-task-name"
				className="new-task-name"
				value={inputValue}
				onChange={(event) => {
					setShowSubmitErrors(false)
					setInputValue(event.target.value)
				}}
			/>
			<button>Ajouter</button>
			{ showSubmitErrors && !inputValue &&
				<p style={{marginTop: 5, color: "red"}}>Veuillez renseigner du texte</p>
			}
		</form>
	)
}