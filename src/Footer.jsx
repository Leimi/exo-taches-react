export default function Footer() {
	return (
		<footer className="Footer Gutter" role="contentinfo">
			<p className="Text">Application réalisée pour la formation B2 dev Campus Academy</p>
		</footer>
	)
}