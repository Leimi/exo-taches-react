export default function Header() {
	return (
		<div className="Header">
			<header className="Header-titleWrapper" role="banner">
				<h1 className="Header-title">Application de liste de tâches</h1>
			</header>

			<nav className="Header-nav" role="navigation">
				<ul>
					<li>
						<a href="#new-task">Ajouter une tâche</a>
					</li>
					<li>
						<a href="#list">Liste des tâches</a>
					</li>
				</ul>
			</nav>
		</div>
	)
}